namespace Tests

open System
open NFA
open DFA

module ContainsSequence = 
    
    let contains0101:NFA = {
        sigma = Seq.toList "01";
        states = [
            "q0";
            "q1";
            "q2";
            "q3"
            "yes"
        ];
        delta = (fun x y -> 
            match (x, y) with
            | ("q0", '0') -> ["q0"; "q1"]
            | ("q0", _  ) -> ["q0"]
            | ("q1", '1') -> ["q0"; "q2"]
            | ("q2", '0') -> ["q0"; "q3"]
            | ("q3", '1') -> ["yes"]
            | ("yes", _) -> ["yes"]
            | _ -> []
        );
        beginState = "q0";
        acceptingStates = ["yes"]

    }

    let test = 
        printfn "Testing contains sequence NFA"
        printfn "Contains '0101' ?"
        printfn "0101: %b" (NFA.acceptsWord contains0101 "0101")
        printfn "0000: %b" (NFA.acceptsWord contains0101 "0000")
        printfn "1010: %b" (NFA.acceptsWord contains0101 "1010")
        printfn "0100: %b" (NFA.acceptsWord contains0101 "0100")
        printfn "00101: %b" (NFA.acceptsWord contains0101 "00101")
        printfn "01011: %b" (NFA.acceptsWord contains0101 "01011")
        printfn "1000101: %b" (NFA.acceptsWord contains0101 "1000101")
        0