﻿
open System

[<EntryPoint>]
let main argv =
    printfn "Finite Automata in F#"
    
    Tests.IsNumeric.test |> ignore
    Tests.ContainsSequence.test |> ignore

    0 // return an integer exit code
