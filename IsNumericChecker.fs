namespace Tests

open System
open DFA
open NFA

module IsNumeric = 

    let numericDFA:DFA = {
        sigma = Seq.toList "01ab";
        states = [
            "yes";
            "no"
        ];
        delta = (fun x y ->
            match (x, y) with
            | ("yes", '0') -> "yes"
            | ("yes", '1') -> "yes"
            | _ -> "no"
        );
        beginState = "yes";
        acceptingStates = ["yes"]
    }

    let numericDFAComplement = DFA.complement numericDFA

    let numericNFA:NFA = {
        sigma = Seq.toList "01ab";
        states = [
            "yes"
        ];
        delta = (fun x y ->
            match (x, y) with
            | ("yes", '0') -> ["yes"]
            | ("yes", '1') -> ["yes"]
            | _ -> []
        );
        beginState = "yes";
        acceptingStates = ["yes"]
    }

    let test = 
        printfn "Testing numeric checker DFA"
        printfn "Is valid DFA: %b" (DFA.validateDFA numericDFA)
        printfn "0101: %b" (DFA.acceptsWord numericDFA "0101")
        printfn "01a1: %b" (DFA.acceptsWord numericDFA "01a1")
        printfn "Empty: %b" (DFA.acceptsWord numericDFA "")
        printfn "Testing numeric checker DFA complement"
        printfn "0101: %b" (DFA.acceptsWord numericDFAComplement "0101")
        printfn "01a1: %b" (DFA.acceptsWord numericDFAComplement "01a1")
        printfn "Testing numeric checker NFA"
        printfn "0101: %A" (NFA.acceptsWord numericNFA "0101")
        printfn "01a1: %b" (NFA.acceptsWord numericNFA "01a1")
        printfn "Empty: %b" (NFA.acceptsWord numericNFA "")
        0